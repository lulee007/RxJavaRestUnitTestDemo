package com.lulee007.rxjavarestunittestdemo.Services;

import org.junit.Test;

import rx.Observable;
import rx.functions.Func1;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class FVideoServiceTest {

    @Test
    public void testFetchVideos() throws Exception {
        FVideoService fVideoService=new FVideoService();
        Observable<FVideoService.VideoListPage> videos=fVideoService.fetchVideos(1,0);
        int size = videos.flatMap(new Func1<FVideoService.VideoListPage, Observable<?>>() {
            @Override
            public Observable<?> call(FVideoService.VideoListPage fVideos) {
                return Observable.from(fVideos.videos);
            }
        }).count().toBlocking().single();
        assertThat(size,equalTo(10));
    }
}