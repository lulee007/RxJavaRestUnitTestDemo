package com.lulee007.rxjavarestunittestdemo.Models;

import com.google.gson.Gson;

/**
 * User: lulee007@live.com
 * Date: 2015-12-04
 * Time: 11:28
 */
public class FVideo {

    /**
     * extra :
     * title : 默默点歌台第三期 《开始懂了》56出品微播江湖片段剪辑
     * video_type_name : 微播江湖
     * video_id : MTAzMTY5MTkw
     * video_type : 1
     * video_src : http://m.56.com/view/id-MTAzMTY5MTkw.html
     * video_duration : 4:30
     * play_times : 0
     * id : 1864
     * icon : http://v1.pfs.56img.com/images/17/9/weibojianghu56i56olo56i56.com_138778918948hd.jpg
     */

    private String extra;
    private String title;
    private String video_type_name;
    private String video_id;
    private int video_type;
    private String video_src;
    private String video_duration;
    private int play_times;
    private int id;
    private String icon;

    public static FVideo objectFromData(String str) {

        return new Gson().fromJson(str, FVideo.class);
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setVideo_type_name(String video_type_name) {
        this.video_type_name = video_type_name;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public void setVideo_type(int video_type) {
        this.video_type = video_type;
    }

    public void setVideo_src(String video_src) {
        this.video_src = video_src;
    }

    public void setVideo_duration(String video_duration) {
        this.video_duration = video_duration;
    }

    public void setPlay_times(int play_times) {
        this.play_times = play_times;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getExtra() {
        return extra;
    }

    public String getTitle() {
        return title;
    }

    public String getVideo_type_name() {
        return video_type_name;
    }

    public String getVideo_id() {
        return video_id;
    }

    public int getVideo_type() {
        return video_type;
    }

    public String getVideo_src() {
        return video_src;
    }

    public String getVideo_duration() {
        return video_duration;
    }

    public int getPlay_times() {
        return play_times;
    }

    public int getId() {
        return id;
    }

    public String getIcon() {
        return icon;
    }
}
