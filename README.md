#RxJavaRestUnitTestDemo
在开发安卓应用时，很多情况下免不了进行数据请求。这里使用Square公司的开源框架Retrofit这是一个用于 Android 平台的，类型安全的 REST 数据请求框架。而RxJava是由Netflix开发的响应式扩展（Reactive Extensions）的Java实现。

本文主要是为了演示在Android Studio中用RxJava和Retrofit进行Http rest api 数据请求的单元测试。

[在Android Studio中为RxJava和Retrofit单元测试
](http://jingyan.baidu.com/article/14bd256e414075bb6d2612ff.html)